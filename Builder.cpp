#include "stdafx.h"
#include "windows.h"
#include <iostream>
#include <vector>

using namespace std;

//��������� ����� � ��������
class Soup
{
  public:
    void print() { 
      cout << "���" << endl; 
    }
};
  
class Potato
{
  public:
    void print() { 
      cout << "���������" << endl; 
    }
};

class Meat
{
  public:
    void print() { 
      cout << "����" << endl; 
    }
};

 
//����� �����
class Course
{
  public:
    vector<Soup> soup; 
    vector<Potato> pot;
    vector<Meat> meat; 
    void print() {   
      int i;
      for(i=0; i<soup.size(); ++i)  soup[i].print();
      for(i=0; i<pot.size(); ++i)  pot[i].print();
      for(i=0; i<meat.size(); ++i)  meat[i].print();
    }
};
  
  
// ����������� ��������� �����
class CourseBuilder
{
  protected: 
    Course* crs_ptr;
  public:    
    CourseBuilder(): crs_ptr(0) {}
    virtual ~CourseBuilder() {}
    virtual void createCourse() {}
    virtual void buildSoup() {}
    virtual void buildPotato() {}
    virtual void buildMeat() {}
    virtual Course* getCourse() { return crs_ptr; }    
};
  
  
// ���������� ��������� ������� �����
class FirstCourseBuilder: public CourseBuilder
{    
  public:    
    void createCourse() { crs_ptr = new Course; }
    void buildSoup() { crs_ptr->soup.push_back( Soup()); }
};
  
  
// ���������� ��������� ������� �����
class SecondCourseBuilder: public CourseBuilder
{    
  public:    
    void createCourse() { crs_ptr = new Course; }
    void buildPotato() { crs_ptr->pot.push_back( Potato()); }
    void buildMeat() { crs_ptr->meat.push_back( Meat()); }    
};
  
  
// ��������
class Director
{    
  public:    
    Course* createCourse( CourseBuilder & builder ) 
    { 
        builder.createCourse();
        builder.buildSoup();
        builder.buildPotato();
        builder.buildMeat();
        return( builder.getCourse());
    }
};


int _tmain(int argc, _TCHAR* argv[])
{  
	setlocale(LC_ALL,"Russian");
    Director dir;
    FirstCourseBuilder fir_builder;
    SecondCourseBuilder sec_builder;
     
    Course * fir_ptr = dir.createCourse( fir_builder);
    Course * sec_ptr = dir.createCourse( sec_builder);
    cout << "������ �����:" << endl;
    fir_ptr->print();
    cout << "\n������ �����:" << endl;
    sec_ptr->print();  
    return 0;
}
